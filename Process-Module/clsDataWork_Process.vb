﻿Imports Ontology_Module
Imports System.Threading
Imports OntologyAppDBConnector
Imports OntologyClasses.BaseClasses

Public Class clsDataWork_Process
    Private objLocalConfig As clsLocalConfig
    Private objDBLevel_Process_Root As OntologyModDBConnector
    Private objDBLevel_Process_Sub As OntologyModDBConnector
    Private objDBLevel_Process_Sub_L1 As OntologyModDBConnector
    Private objDBLevel_Process_Description As OntologyModDBConnector

    Private objOItem_Result_ProcessTree As clsOntologyItem

    Private objThread As Thread
    Private lngCnt As Long

    Public ReadOnly Property Count_Processes As Long
        Get
            Return lngCnt
        End Get
    End Property

    Public ReadOnly Property Result_ProcessTree As clsOntologyItem
        Get
            Return objOItem_Result_ProcessTree
        End Get
    End Property

    Public Function getSubNode(index As Long) As clsObjectTree
        Return objDBLevel_Process_Sub.ObjectTree(index)
    End Function

    Public ReadOnly Property Processes_Public As List(Of clsObjectAtt)
        Get
            objDBLevel_Process_Root.ObjAtts.Sort(Function(U1 As clsObjectAtt, U2 As clsObjectAtt) U1.Name_Object.CompareTo(U2.Name_Object))
            Return objDBLevel_Process_Root.ObjAtts
        End Get
    End Property

    Public Function AddSubNodes(objTreeNode As TreeNode) As clsOntologyItem
        Dim objOItem_Result As clsOntologyItem = objLocalConfig.Globals.LState_Success

        Dim objLNodes = From obj In objDBLevel_Process_Sub.ObjectTree
                        Where obj.ID_Object_Parent = objTreeNode.Name
                        Order By obj.OrderID, obj.Name_Object

        For Each objNodes In objLNodes
            objTreeNode.Nodes.Add(objNodes.ID_Object, _
                                  objNodes.Name_Object, _
                                  objLocalConfig.ImageID_Process, _
                                  objLocalConfig.ImageID_Process)

        Next


        Return objOItem_Result
    End Function

    Public Function get_Next_OrderID(OItem_Process_Parent As clsOntologyItem) As Long
        Dim lngOrderID As Long = 1
        Dim objOItem_Process As clsOntologyItem

        objOItem_Process = New clsOntologyItem(Nothing, Nothing, objLocalConfig.OItem_Type_Process.GUID, objLocalConfig.Globals.Type_Object)


        lngOrderID = objDBLevel_Process_Root.GetDataRelOrderId(OItem_Process_Parent, objOItem_Process, objLocalConfig.OItem_RelationType_superordinate, False)

        lngOrderID = lngOrderID + 1

        Return lngOrderID
    End Function


    Public Function get_Processes() As clsOntologyItem
        Dim objOItem_Process As clsOntologyItem = objLocalConfig.Globals.LState_Success

        objOItem_Result_ProcessTree = objLocalConfig.Globals.LState_Nothing

        Try
            objThread.Abort()
        Catch ex As Exception

        End Try

        objThread = New Thread(AddressOf get_ProcessTree)
        objThread.Start()

        Return objOItem_Process
    End Function

    Public Function get_SubProcesses_L1(OItem_Process_Parent As clsOntologyItem, Optional Name_Process As String = "") As List(Of clsObjectRel)
        Dim objOLProcesses_Search As New List(Of clsObjectRel)
        Dim objOItem_Result As clsOntologyItem
        Dim objOLProcesses As List(Of clsObjectRel)

        If Name_Process = "" Then
            objOLProcesses_Search.Add(New clsObjectRel With {.ID_Object = OItem_Process_Parent.GUID,
                                      .ID_Parent_Other = objLocalConfig.OItem_Type_Process.GUID,
                                      .ID_RelationType = objLocalConfig.OItem_RelationType_superordinate.GUID})

        Else
            objOLProcesses_Search.Add(New clsObjectRel With {.ID_Object = OItem_Process_Parent.GUID,
                                      .ID_Parent_Other = objLocalConfig.OItem_Type_Process.GUID,
                                      .Name_Other = Name_Process,
                                      .ID_RelationType = objLocalConfig.OItem_RelationType_superordinate.GUID})
        End If


        objOItem_Result = objDBLevel_Process_Sub_L1.GetDataObjectRel(objOLProcesses_Search, _
                                                                       doIds:=False)

        If objOItem_Result.GUID = objLocalConfig.Globals.LState_Success.GUID Then
            objOLProcesses = objDBLevel_Process_Sub_L1.ObjectRels
        Else
            objOLProcesses = Nothing
        End If

        Return objOLProcesses
    End Function

    Public Function get_ProcessesPublic(Optional Name_Process As String = "") As List(Of clsObjectAtt)
        Dim objOItem_Result As clsOntologyItem
        Dim objOLProcessesPublic_Search As New List(Of clsObjectAtt)
        Dim objOLProcessesPublic As List(Of clsObjectAtt)

        If Name_Process <> "" Then
            objOLProcessesPublic_Search.Add(New clsObjectAtt(Nothing, _
                                                             Nothing, _
                                                         Name_Process, _
                                                         objLocalConfig.OItem_Type_Process.GUID, _
                                                         Nothing, _
                                                         objLocalConfig.OItem_Attribute_Public.GUID, _
                                                         Nothing, _
                                                         Nothing, _
                                                         Nothing, _
                                                         True, _
                                                         Nothing, _
                                                         Nothing, _
                                                         Nothing, _
                                                         Nothing, _
                                                         objLocalConfig.Globals.DType_Bool.GUID))
        Else
            objOLProcessesPublic_Search.Add(New clsObjectAtt(Nothing, _
                                                         Nothing, _
                                                         Nothing, _
                                                         objLocalConfig.OItem_Type_Process.GUID, _
                                                         Nothing, _
                                                         objLocalConfig.OItem_Attribute_Public.GUID, _
                                                         Nothing, _
                                                         Nothing, _
                                                         Nothing, _
                                                         True, _
                                                         Nothing, _
                                                         Nothing, _
                                                         Nothing, _
                                                         Nothing, _
                                                         objLocalConfig.Globals.DType_Bool.GUID))
        End If


        objOItem_Result = objDBLevel_Process_Root.GetDataObjectAtt(objOLProcessesPublic_Search, _
                                                                     doIds:=False)
        If objOItem_Result.GUID = objLocalConfig.Globals.LState_Success.GUID Then
            objOLProcessesPublic = objDBLevel_Process_Root.ObjAtts
        Else
            objOLProcessesPublic = Nothing
        End If

        Return objOLProcessesPublic
    End Function

    Public Function add_SubNodes_Of_Process_L11(TreeNode_Parent As TreeNode) As clsOntologyItem
        Dim objOItem_Result As clsOntologyItem
        Dim objOItem_Process As clsObjectRel
        Dim objOList_Process As New List(Of clsObjectRel)
        Dim objTreeNodes() As TreeNode
        Dim objTreeNode_Sub As TreeNode

        objOList_Process.Add(New clsObjectRel(TreeNode_Parent.Name, _
                                              Nothing, _
                                              Nothing, _
                                              objLocalConfig.OItem_Type_Process.GUID, _
                                              objLocalConfig.OItem_RelationType_superordinate.GUID, _
                                              objLocalConfig.Globals.Type_Object, _
                                              Nothing, _
                                              Nothing))

        objOItem_Result = objDBLevel_Process_Sub_L1.GetDataObjectRel(objOList_Process, _
                                                                       doIds:=False)

        If objOItem_Result.GUID = objLocalConfig.Globals.LState_Success.GUID Then

            If objDBLevel_Process_Sub_L1.ObjectRels.Count > 0 Then
                objDBLevel_Process_Sub_L1.ObjectRels.Sort(Function(U1 As clsObjectRel, U2 As clsObjectRel) U1.OrderIDNotNull.CompareTo(U2.OrderIDNotNull))
                For Each objOItem_Process In objDBLevel_Process_Sub_L1.ObjectRels
                    objTreeNodes = TreeNode_Parent.Nodes.Find(objOItem_Process.ID_Other, False)
                    If objTreeNodes.Count = 0 Then
                        objTreeNode_Sub = TreeNode_Parent.Nodes.Add(objOItem_Process.ID_Other, _
                                                                    objOItem_Process.Name_Other, _
                                                                    objLocalConfig.ImageID_Process, _
                                                                    objLocalConfig.ImageID_Process)

                    End If
                Next

            End If

            For Each objTreeNode_Sub In TreeNode_Parent.Nodes
                objOItem_Result = add_SubNodes_Of_Process_L11(objTreeNode_Sub)
                If objOItem_Result.GUID = objLocalConfig.Globals.LState_Error.GUID Then
                    Exit For
                End If
            Next

        End If

        Return objOItem_Result
    End Function

    Private Sub get_ProcessTree()
        Dim objOList_Process__Public As New List(Of clsObjectAtt)
        Dim objOItem_Result As clsOntologyItem

        objOItem_Result_ProcessTree = objLocalConfig.Globals.LState_Nothing

        objOList_Process__Public.Add(New clsObjectAtt(Nothing, _
                                                      Nothing, _
                                                      Nothing, _
                                                      objLocalConfig.OItem_Type_Process.GUID, _
                                                      Nothing, _
                                                      objLocalConfig.OItem_Attribute_Public.GUID, _
                                                      Nothing, _
                                                      Nothing, _
                                                      Nothing, _
                                                      True, _
                                                      Nothing, _
                                                      Nothing, _
                                                      Nothing, _
                                                      Nothing, _
                                                      objLocalConfig.Globals.DType_Bool.GUID))

        objOItem_Result = objDBLevel_Process_Root.GetDataObjectAtt(objOList_Process__Public,
                                                                doIds:=False)

        If objOItem_Result.GUID = objLocalConfig.Globals.LState_Success.GUID Then
            objOItem_Result = objDBLevel_Process_Sub.GetDataObjectsTree(objLocalConfig.OItem_Type_Process, _
                                                                            objLocalConfig.OItem_Type_Process, _
                                                                            objLocalConfig.OItem_RelationType_superordinate)

            'objDBLevel_Process_Sub.OList_ObjectTree.Sort(Function(U1 As clsObjectTree, U2 As clsObjectTree) U1.OrderID.CompareTo(U2.OrderID))
            lngCnt = objDBLevel_Process_Sub.ObjectTree.Count
            objOItem_Result_ProcessTree = objOItem_Result
        Else
            objOItem_Result_ProcessTree = objOItem_Result
        End If

    End Sub

    Public Function Get_ProcessDescription(OItem_Process As clsOntologyItem) As clsObjectAtt
        Dim searchDescription = New List(Of clsObjectAtt) From {New clsObjectAtt With {.ID_Object = OItem_Process.GUID,
                                                                                       .ID_AttributeType = objLocalConfig.OItem_Attribute_Description.GUID}}

        Dim objOItem_Result = objDBLevel_Process_Description.GetDataObjectAtt(searchDescription, doIds:=False)

        Dim objOA_Description As clsObjectAtt = Nothing

        If objOItem_Result.GUID = objLocalConfig.Globals.LState_Success.GUID Then
            objOA_Description = objDBLevel_Process_Description.ObjAtts.FirstOrDefault()
        End If

        Return objOA_Description
    End Function

    Public Sub New(LocalConfig As clsLocalConfig)
        objLocalConfig = LocalConfig

        set_DBConnection()
    End Sub

    Private Sub set_DBConnection()
        objDBLevel_Process_Root = New OntologyModDBConnector(objLocalConfig.Globals)
        objDBLevel_Process_Sub = New OntologyModDBConnector(objLocalConfig.Globals)
        objDBLevel_Process_Description = New OntologyModDBConnector(objLocalConfig.Globals)

        objDBLevel_Process_Sub_L1 = New OntologyModDBConnector(objLocalConfig.Globals)
    End Sub
End Class
